'''
Created on 2 oct. 2020

@author: Cesar
'''
import datetime

class Alumno:
    def __init__(self, nombre, edad, carrera, fechaInscripsion):
        if nombre is None:
            self.__nombre = str("")
        else:
            self.__nombre = nombre
        
        if edad is None:
            self.__edad = int()
        else:
            self.__edad = edad
        
        if carrera is None:
            self.__carrera = str("")
        else:
            self.__carrera = carrera
        
        if fechaInscripsion is None:
            self.__fechaInscripcion = datetime.date.today()
        else:
            self.__fechaInscripcion = fechaInscripsion
    
    def getNombre(self):
        return self.__nombre
    
    def setNombre(self, nombre):
        self.__nombre=nombre
    
    def getEdad(self):
        return self.__edad
    
    def setEdad(self, edad):
        self.__edad=edad
    
    def getCarrera(self):
        return self.__carrera
    
    def setCarrera(self, carrera):
        self.__carrera=carrera
    
    def getFechaInscripsion(self):
        return self.__fechaInscripcion
    
    def setFechaInscripsion(self, fechaInscripcion):
        self.__fechaInscripcion=fechaInscripcion

    def __str__(self):
        fecha = datetime.date.today()
        fecha = self.getFechaInscripsion()
        return("Nombre: "+self.getNombre()+", Edad: "+str(self.getEdad())+", Carrera: "+self.getCarrera()+", fecha de Inscripsion: <"+str(fecha.day)+"/"+str(fecha.month)+"/"+str(fecha.year)+">")

class ListaDeAlumnos:
   
    def __init__(self,lisAlumnos):
        if lisAlumnos is None:
            self.__lisAlumnos = dict()
        else:
            self.__lisAlumnos = lisAlumnos
    
    def getLisAlumnos(self):
        return self.__lisAlumnos
    def setLisAlumnos(self, lisAlumnos):
        self.__lisAlumnos=lisAlumnos
        
   
    
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r


    def llenadoC(self):
        e=1
        c=""
        ind=0
        
        while(e==1):
            if(ind==0):
                print("Ingrese la carrera:")
            else:
                c = input().upper()
                
            ind+=1
            if(c=="ISC"or c=="IIA"or c=="IM"or c=="LA"or c=="CP"):
                e=0
                
            else:
                if(ind!=1):
                    print("La carrera que intentas ingresa no existe, intenta otra vez")
                e=1
        return c
    
    def llenadoF(self):
        fechas = []
        e = 1
        
        while(e==1):
            e=0
            print("Ingresa la fecha con el formato (dd/mm/aaaa):")
            fech = str(input())
            
            try:
                fechas.append(int(fech[(5+1):10]))
                fechas.append(int(fech[3:5]))
                fechas.append(int(fech[0:2]))
            except:
                print("El formato es dd/mm/aaaa , intenta de nuevo")
                e=1
                
        fecha = datetime.date(fechas[0],fechas[1],fechas[2])
       
        
        
        return fecha
    


    def llenar(self, c):
        lisAlumnos = dict()
        
        for i in range(c):
            
            print("======== Alumno "+str(i+1)+" ========")
            nombre = str(input("Ingresa el nombre:"))
            print("Ingresa la edad:")
            edad = self.correcion()
            carrera=self.llenadoC()
            fechaInscripsion=self.llenadoF()
            alu = Alumno(nombre, edad, carrera, fechaInscripsion)
            print(alu)
            lisAlumnos[i]=alu
            
        self.setLisAlumnos(lisAlumnos)  
    
    def vaciar(self):
        
        void = dict()
        self.setLisAlumnos(void)
        print("lista vaciada con exito")
    
    def mostrarCarrera(self, c):
        print("Los de la carrera: "+c)
        lisAlumnos = self.getLisAlumnos()
        
        for i in range (len(lisAlumnos)):
            alumnoT = lisAlumnos[i]
            if(alumnoT.getCarrera()==c):
                print(alumnoT)
    
    def calcularPromedioPorEdades(self):
        lisAlumnos = self.getLisAlumnos()
        p = 0.0
        for i in range (len(lisAlumnos)):
            alumnoT = lisAlumnos[i]
            p= p + alumnoT.getEdad()
        p=p/len(lisAlumnos)
        return p
              
    def filtroPorFecha(self, fecha):
        print("En la  fecha:")
        lisAlumnos = self.getLisAlumnos()
        for i in range (len(lisAlumnos)):
            alumnoT = lisAlumnos[i]
            fechas = datetime.date.today()
            fechas = fecha
            if(alumnoT.getFechaInscripcion()>fechas):
                print(alumnoT)

d = dict()
lisA = ListaDeAlumnos(d)
fecha = datetime.date(2016, 8, 10)



opcion = 0
opcionx2 = 0

while(opcion!=6):
    print("Digite 1 para Llenar la lista")
    print("Digite 2 para VAciar la lista")
    print("Digite 3 para Mosrar los alumnos por su carrera")
    print("Digite 4 para calcular el promedio de las edades")
    print("Digite 5 para mostrar a los alumnos que se isncribieron antes de la fecha 10/08/2016")
    print("Digite 6 para ***SALIR***")
      
    opcion = lisA.correcion()
    
    if opcion==1:
        
        
        c =5
        lisA.llenar(c)
        
    elif opcion==2:
        lisA.vaciar()
        
    elif opcion==3:
        carrera=""
        opcionx2=7
        
        while(opcionx2!=6):
            
            
            print("Digite 1 para seleccionar ISC")
            print("Digitre 2 para seleccionar IIA")
            print("Digite 3 para seleccionar IM")
            print("Digite 4 para seleccionar LA")
            print("Digite 5 para seleccionar CP")
            
            opcionx2 = lisA.correcion()()
            
            if(opcionx2==1):
                carrera="ISC"
                opcionx2=6
                
            elif(opcionx2==2):
                carrera="IIA"
                opcionx2=6
                
            elif(opcionx2==3):
                carrera="IM"
                opcionx2=6
                
            elif(opcionx2==4):
                carrera="LA"
                opcionx2=6
                
            elif(opcionx2==5):
                carrera="CP"
                opcionx2=6
                
            else:
                print("Esta carrera no existe, ingresa una que si")
                opcionx2=7
        lisA.mostrarCarrera(carrera);
        
    elif opcion==4:
        print("El promedio de las edades es: "+str(lisA.calcularPromedioPorEdades()))
        
    elif opcion==5:
        lisA.filtroPorFecha(fecha)
        
    elif opcion==6:
        pass
    else:
        print("Ups, opcion invalida, intenta otra vez")
print("Gracias por usar el programa, usted a SALIDO")
